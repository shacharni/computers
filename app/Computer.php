<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Computer extends Model
{
    protected $fillable=['brand','processor','size','quantity_sold','quantity_in_stock',];
    public function user(){
        return $this->hasMany('App\User');
    }
}
