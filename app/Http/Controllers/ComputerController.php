<?php

namespace App\Http\Controllers;
use App\Computer;
use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\DB;
use\Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;

class ComputerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $computers=Computer::all();
        return view("computers.index",['computers'=>$computers]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('manager')) {
            abort(403,"Sorry you are not allowed to create computers :(..");
             } 
  
        return view("computers.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $computer=new Computer();
        $computer->brand=$request->brand;
        $computer->size=$request->size;
        $computer->processor=$request->processor;
        $computer->quantity_sold=$request->quantity_sold;
        $computer->quantity_in_stock=$request->quantity_in_stock;
       // $computer->user_id=$id;
        $computer->save();
        return redirect('computers');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)

    {
        if (Gate::denies('manager')) {
            abort(403,"Are you a hacker or what?");
              }

        $computer=Computer::find($id);
        return view('computers.edit',['computer'=>$computer]);
 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $computer = Computer::findOrFail($id);
             
              if (Gate::denies('manager')) {
                    if ($request->has('brand'))
                          abort(403,"You are not allowed to edit todos..");
                }   

      //  $computer=Computer::find($id);
        $computer->update($request->all());
        return redirect ('computers');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('manager')) {
            abort(403,"Are you a hacker or what?");
        }

        $computer=Computer::find($id);
        $computer->delete();
        return redirect ('computers');

    }
    public function buy($id,$quantity_sold,$quantity_in_stock){

        if (Gate::denies('customer')) {
            abort(403,"Are you a hacker or what?");
              }
        $computer=Computer::find($id);    
        $computer->quantity_sold=$quantity_sold;
        $computer->quantity_in_stock=$quantity_in_stock;
        $computer->save();
      return redirect ('computers');
}
}
