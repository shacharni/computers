<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComputersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('computers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('brand', ['hp', 'dell','asus','Lenovo']);
            $table->enum('processor', ['i3','i5','i7']);
            $table->enum('size', ['15.6', '13.1']);
            $table->integer('quantity_sold')->default('0');
            $table->integer('quantity_in_stock')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('computers');
    }
}
