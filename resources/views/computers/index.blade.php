@extends('layouts.app')

@section('content')


<!DOCTYPE html>
<html>
<h1> this is a computer list</h1>
@can('manager')<a href="{{route('computers.create')}}">Create a new computer</a>@endcan
    <head>
    </head>
    <body>
    <table style="width:50%">
  <tr>
    <th>id</th>
    <th>brand</th> 
    <th>processor</th>
    <th>size</th>
  </tr>
  @foreach($computers as $computer)
  <tr>
    <td>{{$computer->id}}</td>
    @if(($computer->quantity_sold)>10 &&($computer->quantity_in_stock)!==0)
     <td><u>{{$computer->brand}}</u></td>
     @elseif(($computer->quantity_in_stock)===0)
     <td>sold out</td>
     @elseif(($computer->quantity_in_stock)===0)&&($computer->quantity_sold)>10)
     <td>sold out</td>
   @else
    <td>{{$computer->brand}}</td>
    @endif
    
    <td>{{$computer->processor}}</td>
    <td>{{$computer->size}}</td>
    @cannot('manager')<td>
    @if(($computer->quantity_in_stock)!==0)
    <a href="{{route('computers.buy', ['id' =>$computer->id,'quantity_sold'=>$computer->quantity_sold+1,
    'quantity_in_stock'=>$computer->quantity_in_stock-1 ])}}">
    buy</a>
    @else 
    buy
    @endif
    </td>@endcannot
  @can('manager')  <td><a href="{{route('computers.edit',$computer->id)}}">edit </td> @endcan
  @can('manager') <td><form method='post' action="{{action('ComputerController@destroy',$computer->id)}}">
    @csrf
    @method('DELETE')

    <div class = "form-group">
        <input type="submit" class="form-controll" name="submit" value="delete">
    </div>
</form>

  </td>@endcan
  </tr>
  @endforeach
</table>
   
   </body>
</html>
@endsection