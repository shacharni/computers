@extends('layouts.app')

@section('content')

<h1>Create a new computer</h1>
<form method='post' action="{{action('ComputerController@store')}}">
    {{csrf_field()}}

    <div class="form-group">
    <h2>Add a computer</h2>
    <h3>brand</h3>
    <select name="brand">
    <option value="hp">hp</option>
    <option value="dell">dell</option>
    <option value="asus">asus</option>
    <option value="Lenovo">Lenovo</option>
  </select>
  <br><br>
  <h3>processor</h3>
    <select name="processor">
    <option value="i3">i3</option>
    <option value="i5">i5</option>
    <option value="i7">i7</option>
  </select>
  <br><br>
  <h3>size</h3>
    <select name="size">
    <option value="15.6">15.6</option>
    <option value="13.1">13.1</option>
  </select>
  <br><br>
  <label for ="quantity_sold">quantity_sold </label>
        <input type="text" class= "form-control" name="quantity_sold">
        <label for ="title"> quantity_in_stock </label>
        <input type="quantity_in_stock" class= "form-control" name=" quantity_in_stock">

 
    <div class = "form-group">
        <input type="submit" class="form-control" name="submit" value="save">
    </div>
</form>
@endsection