<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::resource('computers','ComputerController')->middleware('auth');
// Route::resource('computers','ComputerController');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('computers/{id}/{quantity_sold}/{quantity_in_stock}','ComputerController@buy')->name('computers.buy');

